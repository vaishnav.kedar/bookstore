package com.BookStore.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BookStoreController {

	// calling the Login page as the home by using the mapping
	@RequestMapping("/")
	public String Login() {
		System.out.println("inside the Login()");
		return "login";
	}

	@RequestMapping(value= "/login")
	public ModelAndView Welcome(@RequestParam("uname") String uname, @RequestParam("pwd") String pwd) {

		System.out.println("You're inside the Welcome()");
		ModelAndView mv = new ModelAndView("Welcome");
		mv.addObject("uname", uname);
		mv.addObject("pwd", pwd);
		return mv;
	}
	@RequestMapping("/register")
	public String Register() {
		System.out.println("inside the Register()");
		return "Register";
	}
	
	@RequestMapping(value = "/registered")
	public ModelAndView Register(@RequestParam("uname") String uname, @RequestParam("pwd") String pwd,@RequestParam("cpwd") String cpwd)
	{
	System.out.println("Inside Register() for navigating to welcome");
	ModelAndView mv= new ModelAndView();
	mv.setViewName("Welcome");
	mv.addObject("uname",uname);
	mv.addObject("pwd",pwd);
	return mv;
	}
	@RequestMapping("/Forgotpwd")
    public String Change_pwd()
   {
		System.out.println("Inside Forgot Change_pwd()");
	return "Forgotpwd";
   }
	
	@RequestMapping(value = "/cpwd_change")
	public ModelAndView Login_after_pwd_changed(@RequestParam("uname") String uname) {
		System.out.println("inside the Login_after_pwd_changed()");
		ModelAndView mv= new ModelAndView();
		mv.setViewName("cpwd_change");
		mv.addObject("uname",uname);
		return mv;
	}
	
	@RequestMapping("/relogin")
	public String Relogin() {
		System.out.println("inside the Relogin()");
		return "login";
	}
}
