package com.BookStore.Startup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.BookStore.Controller.BookStoreController;

@SpringBootApplication
//@ComponentScan({"com.BookStore.Controller"})
@Import({BookStoreController.class})
public class BookStoreApplication extends SpringBootServletInitializer{

	
	public static void main(String[] args) {
		SpringApplication.run(BookStoreApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BookStoreApplication.class);
	}
}
