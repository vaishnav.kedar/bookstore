<%@ page  contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Change Password</title>
            </head>
            <style>
input[type=text], input[type=password] {
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 2px solid  #000000;
  box-sizing: border-box;
}
      
form
{
 margin:50px 0px; padding:0px;
   
    align:center;
}      
      
      
 button {
 
  background-color:  #4080bf;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  
}


/* Add a hover effect for buttons */
button:hover {
  opacity: 0.8;
}
            </style>
    <body>
   <form method="post" action="cpwd_change" autocomplete="off" name="pwd_changed">
       <table align="center">
           <tbody><div class="formcontainer">
             <p><center><b><h2>Change Password</h2></b></center></p>
                      <tr> <td>    <b>Username</b> </td>
                           <td> <input type="text" placeholder="Enter Username" name="uname" style="width: 300px;" required> </td> </tr>
                                             
                       <tr> <td>  <b>Password</b> </td>
                             <td><input type="password" placeholder="Enter Password" name="pwd" minlength="4" style="width: 300px;" required></td> </tr>
 
                       <tr> <td>  <b>Confirm Password</b> </td>
                             <td><input type="password" placeholder="Enter Confirm Password" name="cpwd" minlength="4" style="width: 300px;" required></td> </tr>
                       
                       <tr> <td>    <button type="submit" name="Change_pwd" style="width: 200px;">Change Password</button> </td> </tr>
                     <label>
                     
    </div>
    </tbody>
    </table>
</form>
        </body>
</html>