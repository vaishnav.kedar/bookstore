<%@ page  contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <title>Login</title>
            </head>
      <style>
      
      
      
input[type=text], input[type=password] {
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 2px solid  #000000;
  box-sizing: border-box;
}
      
form
{
 margin:50px 0px; padding:0px;
   
    align:center;
}      
      
      
 button {
 
  background-color:  #4080bf;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  
}


/* Add a hover effect for buttons */
button:hover {
  opacity: 0.8;
}


/* Styling the formcontainer */
.formcontainer {
  padding: 16px;
}
      
      </style>     
    <body>
   <form method="post" action="login" autocomplete="off" >
       <table align="center">
           <tbody><div class="formcontainer">
           <p><center><b><h2>Login</h2></b></center></p>
                      <tr> <td>    <b>Username</b> </td>
                           <td> <input type="text" placeholder="Enter Username" name="uname" style="width: 300px;" required> </td> </tr>

                       <tr> <td>  <b>Password</b> </td>
                             <td><input type="password" placeholder="Enter Password" name="pwd" minlength="4" style="width: 300px;" required></td> </tr>
 
                       <tr> <td>   <button type="submit" name="login" style="width: 200px;">Login</button> </td> 
                      
                     <label>
                      <tr> <td>   <input type="checkbox"  name="remember"> Remember me
                      </label> </td>
                      <label>
                      <td> <a href="Forgotpwd">Forgot Password?</a></td> </tr>
                         </div>
                          </tbody>
    </table>
</form>
<p><center>Click here to <a href = "register"> Register</a></center></p>
        </body>
</html>